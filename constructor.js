var studentsAndPoints = ['Алексей Петров', 0, 'Ирина Овчинникова', 60, 'Глеб Стукалов', 30, 'Антон Павлович', 30, 'Виктория Заровская', 30, 'Алексей Левенец', 70, 'Тимур Вамуш', 30, 'Евгений Прочан', 60, 'Александр Малов', 0];
// Задание 1
function Student(name, point) {
  this.name = name;
  this.point = point;
};

Student.prototype.show = function() {
  console.log('Студент %s набрал %d баллов', this.name, this.point)};

// Задание 2
function StudentList(nameList, studentsAndPoints) {
  this.nameList = nameList;
  if (studentsAndPoints !== undefined) {
    for (var i=0, imax = studentsAndPoints.length;i < imax; i+=2){
      this.push(new Student(studentsAndPoints[i], studentsAndPoints[i+1]));
    };
  }
};

StudentList.prototype = Object.create(Array.prototype);
StudentList.prototype.constructor = StudentList;
StudentList.prototype.add = function(name, point){
 this.push(new Student(name, point))};

// Задание 3
var hj2 = new StudentList('HJ-2', studentsAndPoints);

// Задание 4
hj2.add('Александр Самарцев', 60);
hj2.add('Борис Кисель', 30);
hj2.add('Станислав Жданов', 40);

// Задание 5
var html7 = new StudentList('HTML-7');

html7.add('Елизавета Проходская', 20);
html7.add('Константин Львовский', 30);
html7.add('Анастасия Зеленская', 70);

// Задание 6
StudentList.prototype.show = function(){
  console.log('Группа %s (%d  студентов):', this.nameList, this.length);
  this.forEach(function(item){
    item.show();
  });
};
hj2.show();
html7.show();

// Задание 7
html7.splice(2,1).forEach(function(item){
  hj2.push(item);
}
);
